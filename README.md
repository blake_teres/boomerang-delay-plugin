# README #

Boomerang VST/AU/Standalone Delay Plugin

![Boomerang_Screenshot.png](https://bitbucket.org/repo/RbMknA/images/1638426232-Boomerang_Screenshot.png)

Senior Undergraduate Fall Capstone Project

### What is this repository for? ###

* A simple delay plugin that offers millisecond and DAW bpm sync.
* Still has some bugs, should be fixed soon.

### Known issues ###

* Sync option not working in some DAWs (hardcoded to 120 BPM for now)
* Upon closing the plugin window, parameters reset.
* Modulation effect sounds bad (for now!)

### Who do I talk to? ###

* Blake Teres
* American University 2017
* blaketeres@gmail.com
* All feedback welcome!